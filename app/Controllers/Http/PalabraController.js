'use strict';

const Palabra = use('App/Models/Palabra');
const Batch = use('App/Models/Batch');

class PalabraController {
  async index ({ response }) {
    const palabras = await Palabra.all();
    const batch = await Batch.first();

    return response.status(200).json({
      data: palabras,
      batch: batch ? batch.batch : 0,
      code: 200
    });
  }

  async store ({ request, response }) {
    const body = request.only([ 'palabra1', 'palabra2', 'palabra3' ]);

    /** recorrer las palabras que fueron introducidas */
    let palabraEncontrada = null;
    await Object.keys(body).forEach(async (palabra) => {
      /** buscarlas en la base de datos */
      palabraEncontrada = await Palabra.findBy('palabra', body[palabra]);

      if (!palabraEncontrada) {
        /** si la palabra no se encuentra se crea y se guarda en la base */
        await Palabra.create({ palabra: body[palabra] });
      } else {
        /** si la palabra existe entonces la frecuencia incrementa más 1 */
        palabraEncontrada.merge({
          frecuencia: palabraEncontrada.frecuencia + 1
        });

        /** salvar los cambios en la base de datos */
        await palabraEncontrada.save();
      }
    });

    const batch = await Batch.first();

    if (batch) {
      batch.merge({
        batch: batch.batch + 1
      });

      await batch.save();
    } else {
      await Batch.create({});
    }

    return response.status(200).send(null);
  }
}

module.exports = PalabraController;
