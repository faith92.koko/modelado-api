'use strict'

const Schema = use('Schema')

class BatchSchema extends Schema {
  up () {
    this.create('batches', (table) => {
      table.increments()
      table.integer('batch').defaultTo(1);
      table.timestamps()
    })
  }

  down () {
    this.drop('batches')
  }
}

module.exports = BatchSchema
