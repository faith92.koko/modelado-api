'use strict';

const Schema = use('Schema');

class PalabraSchema extends Schema {
  up () {
    this.create('palabras', (table) => {
      table.increments();
      table.string('palabra', 255);
      table.integer('frecuencia').defaultTo(1);
      table.timestamps();
    });
  }

  down () {
    this.drop('palabras');
  }
}

module.exports = PalabraSchema
